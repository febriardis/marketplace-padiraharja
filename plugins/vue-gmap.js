import Vue from 'vue'
import * as GmapVue from 'gmap-vue'

Vue.use(GmapVue, {
  load: {
    key: 'AIzaSyBPx02inVenk0bE7hU-cjtWlZpIrF8tDsQ',
    installComponents: true,
    libraries: 'places',
  },
})
