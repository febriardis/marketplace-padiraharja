// Initialize Firebase
import firebase from 'firebase/app'
import 'firebase/messaging'

firebase.initializeApp({
  apiKey: 'AIzaSyAQ2w9P1EOSWUQjhTuWoqjdbbBOC2qGK78',
  authDomain: 'padiraharjadeveloper-e6421.firebaseapp.com',
  projectId: 'padiraharjadeveloper-e6421',
  storageBucket: 'padiraharjadeveloper-e6421.appspot.com',
  messagingSenderId: '662752658905',
  appId: '1:662752658905:web:7a5a5d40e8bb0f102185c8',
  measurementId: 'G-ZL88QCGEQZ',
})

navigator.serviceWorker
  .register('firebase-messaging-sw.js')
  .then((registration) => {
    const messaging = firebase.messaging()
    messaging.useServiceWorker(registration)
  })
  .catch((err) => {
    console.log(err)
  })
// fiebase configs end

// import firebase from 'firebase/app'
// import 'firebase/messaging'

// const fire = firebase.initializeApp({
//   apiKey: 'AIzaSyAQ2w9P1EOSWUQjhTuWoqjdbbBOC2qGK78',
//   authDomain: 'padiraharjadeveloper-e6421.firebaseapp.com',
//   projectId: 'padiraharjadeveloper-e6421',
//   storageBucket: 'padiraharjadeveloper-e6421.appspot.com',
//   messagingSenderId: '662752658905',
//   appId: '1:662752658905:web:7a5a5d40e8bb0f102185c8',
//   measurementId: 'G-ZL88QCGEQZ',
// })

// export default fire
