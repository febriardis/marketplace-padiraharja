import Vue from 'vue'
import numeral from 'numeral'
import moment from 'moment'

Vue.filter('price', function (integer) {
  return numeral(integer).format('0,0')
})

Vue.filter('formatDate', (value, customFormat) => {
  // Convert to Regular Date Format
  // const timestamp = moment(value).unix()
  // const d = new Date(timestamp * 1000)
  // return moment(d).format(customFormat || 'DD-MM-YYYY')
  return moment(value).format(customFormat || 'DD/MM/YYYY')
})

Vue.filter('lowercase', function (value) {
  if (!value) return ''
  return value.toString().toLowerCase()
})

Vue.filter('capitalize', function (value) {
  if (typeof value !== 'string') return ''
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('compareDuration', (start, end) => {
  const dS = new Date(start)
  const dE = new Date(end)
  return moment(dE).diff(dS, 'seconds') - 7
})

Vue.filter('imageView', function (value) {
  const pathArray = value.split('/')
  const protocol = pathArray[0]
  if (protocol === 'https:') {
    return value
  } else {
    return `https://api.padiraharja.com/${value}`
  }
})

Vue.filter('imageViewBanner', function (value) {
  const pathArray = value.split('/')
  const protocol = pathArray[0]
  if (protocol === 'https:') {
    return value
  } else {
    return `https://adminapi.padiraharja.com/${value}`
  }
})

export default Vue.options.filters
Vue.prototype.$filters = Vue.options.filters
